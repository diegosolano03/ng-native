"use strict";
var core_1 = require("@angular/core");
var DashboardComponent = (function () {
    function DashboardComponent() {
        this.checkProp = false;
    }
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'dashboard',
        templateUrl: "dashboard.component.html",
    })
], DashboardComponent);
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGFzaGJvYXJkLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRhc2hib2FyZC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHNDQUEwQztBQU8xQyxJQUFhLGtCQUFrQjtJQUwvQjtRQU1XLGNBQVMsR0FBWSxLQUFLLENBQUM7SUFDdEMsQ0FBQztJQUFELHlCQUFDO0FBQUQsQ0FBQyxBQUZELElBRUM7QUFGWSxrQkFBa0I7SUFMOUIsZ0JBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtRQUNuQixRQUFRLEVBQUUsV0FBVztRQUNyQixXQUFXLEVBQUUsMEJBQTBCO0tBQzFDLENBQUM7R0FDVyxrQkFBa0IsQ0FFOUI7QUFGWSxnREFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHNlbGVjdG9yOiAnZGFzaGJvYXJkJyxcbiAgICB0ZW1wbGF0ZVVybDogXCJkYXNoYm9hcmQuY29tcG9uZW50Lmh0bWxcIixcbn0pXG5leHBvcnQgY2xhc3MgRGFzaGJvYXJkQ29tcG9uZW50e1xuICAgIHB1YmxpYyBjaGVja1Byb3A6IGJvb2xlYW4gPSBmYWxzZTtcbn1cbiJdfQ==