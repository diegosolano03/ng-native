"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var DetailComponent = (function () {
    function DetailComponent(router, params) {
        var _this = this;
        this.router = router;
        this.params = params;
        this.items$ = new BehaviorSubject_1.BehaviorSubject([]);
        console.log(params.context.msg);
        var items = [
            { title: 'NativeScript' },
            { title: 'Angular' },
            { title: 'TypeScript' },
            { title: 'JavaScript' }
        ];
        var cnt = 0;
        var timer = setInterval(function () {
            if (cnt < 4) {
                _this.items$.next(_this.items$.getValue().concat([items[cnt]]));
                cnt++;
            }
            else {
                clearInterval(timer);
            }
        }, 1000);
    }
    DetailComponent.prototype.onItemTap = function (e) {
        for (var key in e) {
            console.log(key + ": " + e[key]);
        }
    };
    DetailComponent.prototype.goBack = function () {
        this.router.back();
    };
    DetailComponent.prototype.close = function () {
        this.params.closeCallback();
    };
    return DetailComponent;
}());
DetailComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'detail',
        templateUrl: "detail.component.html",
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, dialogs_1.ModalDialogParams])
], DetailComponent);
exports.DetailComponent = DetailComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV0YWlsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImRldGFpbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHNDQUEwQztBQUMxQyxzREFBK0Q7QUFDL0QsbUVBQTRFO0FBQzVFLHdEQUF1RDtBQVF2RCxJQUFhLGVBQWU7SUFJeEIseUJBQW9CLE1BQXdCLEVBQVUsTUFBeUI7UUFBL0UsaUJBa0JDO1FBbEJtQixXQUFNLEdBQU4sTUFBTSxDQUFrQjtRQUFVLFdBQU0sR0FBTixNQUFNLENBQW1CO1FBRnhFLFdBQU0sR0FBZ0MsSUFBSSxpQ0FBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBR2pFLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNoQyxJQUFJLEtBQUssR0FBVTtZQUNmLEVBQUUsS0FBSyxFQUFFLGNBQWMsRUFBQztZQUN4QixFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUM7WUFDbkIsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFDO1lBQ3RCLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBQztTQUN6QixDQUFDO1FBQ0YsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBRVosSUFBSSxLQUFLLEdBQUcsV0FBVyxDQUFDO1lBQ3BCLEVBQUUsQ0FBQSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQSxDQUFDO2dCQUNSLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFPLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLFNBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFFLENBQUM7Z0JBQzVELEdBQUcsRUFBRSxDQUFDO1lBQ1YsQ0FBQztZQUFBLElBQUksQ0FBQSxDQUFDO2dCQUNGLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN6QixDQUFDO1FBQ0wsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2IsQ0FBQztJQUVNLG1DQUFTLEdBQWhCLFVBQWlCLENBQUM7UUFDZCxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQSxDQUFDO1lBQ2YsT0FBTyxDQUFDLEdBQUcsQ0FBSSxHQUFHLFVBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBRyxDQUFDLENBQUE7UUFDcEMsQ0FBQztJQUNMLENBQUM7SUFFTSxnQ0FBTSxHQUFiO1FBQ0ksSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRU0sK0JBQUssR0FBWjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUNMLHNCQUFDO0FBQUQsQ0FBQyxBQXJDRCxJQXFDQztBQXJDWSxlQUFlO0lBTjNCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDbkIsUUFBUSxFQUFFLFFBQVE7UUFDbEIsV0FBVyxFQUFFLHVCQUF1QjtLQUN2QyxDQUFDO3FDQU04Qix5QkFBZ0IsRUFBa0IsMkJBQWlCO0dBSnRFLGVBQWUsQ0FxQzNCO0FBckNZLDBDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgTW9kYWxEaWFsb2dQYXJhbXMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9kaXJlY3RpdmVzL2RpYWxvZ3MnO1xuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcy9CZWhhdmlvclN1YmplY3QnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHNlbGVjdG9yOiAnZGV0YWlsJyxcbiAgICB0ZW1wbGF0ZVVybDogXCJkZXRhaWwuY29tcG9uZW50Lmh0bWxcIixcbn0pXG5cbmV4cG9ydCBjbGFzcyBEZXRhaWxDb21wb25lbnR7XG5cbiAgICBwdWJsaWMgaXRlbXMkOiBCZWhhdmlvclN1YmplY3Q8QXJyYXk8YW55Pj4gPSBuZXcgQmVoYXZpb3JTdWJqZWN0KFtdKTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHBhcmFtczogTW9kYWxEaWFsb2dQYXJhbXMpe1xuICAgICAgICBjb25zb2xlLmxvZyhwYXJhbXMuY29udGV4dC5tc2cpO1xuICAgICAgICBsZXQgaXRlbXM6IGFueVtdID0gW1xuICAgICAgICAgICAgeyB0aXRsZTogJ05hdGl2ZVNjcmlwdCd9LFxuICAgICAgICAgICAgeyB0aXRsZTogJ0FuZ3VsYXInfSxcbiAgICAgICAgICAgIHsgdGl0bGU6ICdUeXBlU2NyaXB0J30sXG4gICAgICAgICAgICB7IHRpdGxlOiAnSmF2YVNjcmlwdCd9XG4gICAgICAgIF07XG4gICAgICAgIGxldCBjbnQgPSAwO1xuICAgICAgICBcbiAgICAgICAgbGV0IHRpbWVyID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgICAgICAgaWYoY250IDwgNCl7XG4gICAgICAgICAgICAgICAgdGhpcy5pdGVtcyQubmV4dChbIC4uLiB0aGlzLml0ZW1zJC5nZXRWYWx1ZSgpLCBpdGVtc1tjbnRdXSk7XG4gICAgICAgICAgICAgICAgY250Kys7XG4gICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICBjbGVhckludGVydmFsKHRpbWVyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgMTAwMCk7XG4gICAgfVxuXG4gICAgcHVibGljIG9uSXRlbVRhcChlKSB7XG4gICAgICAgIGZvciAobGV0IGtleSBpbiBlKXtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGAke2tleX06ICR7ZVtrZXldfWApXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgZ29CYWNrKCkge1xuICAgICAgICB0aGlzLnJvdXRlci5iYWNrKCk7XG4gICAgfVxuXG4gICAgcHVibGljIGNsb3NlKCkge1xuICAgICAgICB0aGlzLnBhcmFtcy5jbG9zZUNhbGxiYWNrKCk7XG4gICAgfVxufVxuIl19