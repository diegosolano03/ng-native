"use strict";
var core_1 = require("@angular/core");
var dialogs_1 = require("nativescript-angular/directives/dialogs");
var detail_component_1 = require("../detail/detail.component");
var AboutComponent = (function () {
    function AboutComponent(modal, viewRef) {
        this.modal = modal;
        this.viewRef = viewRef;
    }
    ;
    AboutComponent.prototype.openModal = function () {
        var options = {
            context: { msg: 'Hello' },
            viewContainerRef: this.viewRef
        };
        this.modal.showModal(detail_component_1.DetailComponent, options).then(function (result) {
            console.log(result);
        });
    };
    return AboutComponent;
}());
AboutComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'about',
        templateUrl: "about.component.html",
    }),
    __metadata("design:paramtypes", [dialogs_1.ModalDialogService, core_1.ViewContainerRef])
], AboutComponent);
exports.AboutComponent = AboutComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWJvdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYWJvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxzQ0FBNEQ7QUFDNUQsbUVBQWlHO0FBQ2pHLCtEQUE2RDtBQVE3RCxJQUFhLGNBQWM7SUFDdkIsd0JBQXFCLEtBQXlCLEVBQVUsT0FBeUI7UUFBNUQsVUFBSyxHQUFMLEtBQUssQ0FBb0I7UUFBVSxZQUFPLEdBQVAsT0FBTyxDQUFrQjtJQUFHLENBQUM7SUFBQSxDQUFDO0lBRS9FLGtDQUFTLEdBQWhCO1FBQ0ksSUFBSSxPQUFPLEdBQXVCO1lBQzlCLE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUU7WUFDekIsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLE9BQU87U0FDakMsQ0FBQztRQUNGLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLGtDQUFlLEVBQUUsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUUsTUFBTTtZQUN4RCxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNMLHFCQUFDO0FBQUQsQ0FBQyxBQVpELElBWUM7QUFaWSxjQUFjO0lBTjFCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDbkIsUUFBUSxFQUFFLE9BQU87UUFDakIsV0FBVyxFQUFFLHNCQUFzQjtLQUN0QyxDQUFDO3FDQUc4Qiw0QkFBa0IsRUFBbUIsdUJBQWdCO0dBRHhFLGNBQWMsQ0FZMUI7QUFaWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NvbnRhaW5lclJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTW9kYWxEaWFsb2dTZXJ2aWNlLCBNb2RhbERpYWxvZ09wdGlvbnMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9kaXJlY3RpdmVzL2RpYWxvZ3MnO1xuaW1wb3J0IHsgRGV0YWlsQ29tcG9uZW50IH0gZnJvbSAnLi4vZGV0YWlsL2RldGFpbC5jb21wb25lbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHNlbGVjdG9yOiAnYWJvdXQnLFxuICAgIHRlbXBsYXRlVXJsOiBcImFib3V0LmNvbXBvbmVudC5odG1sXCIsXG59KVxuXG5leHBvcnQgY2xhc3MgQWJvdXRDb21wb25lbnR7XG4gICAgY29uc3RydWN0b3IoIHByaXZhdGUgbW9kYWw6IE1vZGFsRGlhbG9nU2VydmljZSwgcHJpdmF0ZSB2aWV3UmVmOiBWaWV3Q29udGFpbmVyUmVmKXsgfTtcblxuICAgIHB1YmxpYyBvcGVuTW9kYWwoKXtcbiAgICAgICAgbGV0IG9wdGlvbnM6IE1vZGFsRGlhbG9nT3B0aW9ucyA9IHtcbiAgICAgICAgICAgIGNvbnRleHQ6IHsgbXNnOiAnSGVsbG8nIH0sXG4gICAgICAgICAgICB2aWV3Q29udGFpbmVyUmVmOiB0aGlzLnZpZXdSZWZcbiAgICAgICAgfTtcbiAgICAgICAgdGhpcy5tb2RhbC5zaG93TW9kYWwoRGV0YWlsQ29tcG9uZW50LCBvcHRpb25zKS50aGVuKCggcmVzdWx0ICkgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0KTtcbiAgICAgICAgfSk7XG4gICAgfVxufVxuIl19