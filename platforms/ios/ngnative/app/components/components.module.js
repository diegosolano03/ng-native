"use strict";
var platform_1 = require("nativescript-angular/platform");
var router_1 = require("nativescript-angular/router");
var core_1 = require("@angular/core");
var home_component_1 = require("./home/home.component");
var dashboard_component_1 = require("./dashboard/dashboard.component");
var about_component_1 = require("./about/about.component");
var detail_component_1 = require("./detail/detail.component");
var COMPONENTS = [
    home_component_1.HomeComponent,
    dashboard_component_1.DashboardComponent,
    about_component_1.AboutComponent,
    detail_component_1.DetailComponent
];
exports.routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: home_component_1.HomeComponent,
        children: [
            { path: '', component: dashboard_component_1.DashboardComponent },
            { path: 'about', component: about_component_1.AboutComponent }
        ]
    },
    {
        path: 'detail',
        component: detail_component_1.DetailComponent
    }
];
var ComponentsModule = (function () {
    function ComponentsModule() {
    }
    return ComponentsModule;
}());
ComponentsModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_1.NativeScriptModule,
            router_1.NativeScriptRouterModule
        ],
        declarations: [
            COMPONENTS
        ],
        exports: [
            COMPONENTS,
            platform_1.NativeScriptModule,
            router_1.NativeScriptRouterModule
        ]
    })
], ComponentsModule);
exports.ComponentsModule = ComponentsModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50cy5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsMERBQW1FO0FBQ25FLHNEQUF1RTtBQUN2RSxzQ0FBeUM7QUFHekMsd0RBQXNEO0FBQ3RELHVFQUFxRTtBQUNyRSwyREFBeUQ7QUFDekQsOERBQTREO0FBRTVELElBQU0sVUFBVSxHQUFVO0lBQ3RCLDhCQUFhO0lBQ2Isd0NBQWtCO0lBQ2xCLGdDQUFjO0lBQ2Qsa0NBQWU7Q0FDbEIsQ0FBQztBQUVXLFFBQUEsTUFBTSxHQUFXO0lBQzFCO1FBQ0ksSUFBSSxFQUFFLEVBQUU7UUFDUixVQUFVLEVBQUUsT0FBTztRQUNuQixTQUFTLEVBQUUsTUFBTTtLQUNwQjtJQUNEO1FBQ0ksSUFBSSxFQUFFLE1BQU07UUFDWixTQUFTLEVBQUUsOEJBQWE7UUFDeEIsUUFBUSxFQUFFO1lBQ04sRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSx3Q0FBa0IsRUFBRTtZQUMzQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLGdDQUFjLEVBQUU7U0FDL0M7S0FDSjtJQUNEO1FBQ0ksSUFBSSxFQUFFLFFBQVE7UUFDZCxTQUFTLEVBQUUsa0NBQWU7S0FDN0I7Q0FDSixDQUFDO0FBZ0JGLElBQWEsZ0JBQWdCO0lBQTdCO0lBQWdDLENBQUM7SUFBRCx1QkFBQztBQUFELENBQUMsQUFBakMsSUFBaUM7QUFBcEIsZ0JBQWdCO0lBZDVCLGVBQVEsQ0FBQztRQUNSLE9BQU8sRUFBRTtZQUNILDZCQUFrQjtZQUNsQixpQ0FBd0I7U0FDM0I7UUFDRCxZQUFZLEVBQUU7WUFDVixVQUFVO1NBQ2I7UUFDRCxPQUFPLEVBQUU7WUFDTixVQUFVO1lBQ1QsNkJBQWtCO1lBQ2xCLGlDQUF3QjtTQUMzQjtLQUNKLENBQUM7R0FDVyxnQkFBZ0IsQ0FBSTtBQUFwQiw0Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOYXRpdmVTY3JpcHRNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcGxhdGZvcm1cIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlcyB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5cbmltcG9ydCB7IEhvbWVDb21wb25lbnQgfSBmcm9tIFwiLi9ob21lL2hvbWUuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBEYXNoYm9hcmRDb21wb25lbnQgfSBmcm9tIFwiLi9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgQWJvdXRDb21wb25lbnQgfSBmcm9tIFwiLi9hYm91dC9hYm91dC5jb21wb25lbnRcIjtcbmltcG9ydCB7IERldGFpbENvbXBvbmVudCB9IGZyb20gXCIuL2RldGFpbC9kZXRhaWwuY29tcG9uZW50XCI7XG5cbmNvbnN0IENPTVBPTkVOVFM6IGFueVtdID0gW1xuICAgIEhvbWVDb21wb25lbnQsXG4gICAgRGFzaGJvYXJkQ29tcG9uZW50LFxuICAgIEFib3V0Q29tcG9uZW50LFxuICAgIERldGFpbENvbXBvbmVudFxuXTtcblxuZXhwb3J0IGNvbnN0IHJvdXRlczogUm91dGVzID0gW1xuICAgIHtcbiAgICAgICAgcGF0aDogJycsXG4gICAgICAgIHJlZGlyZWN0VG86ICcvaG9tZScsXG4gICAgICAgIHBhdGhNYXRjaDogJ2Z1bGwnXG4gICAgfSxcbiAgICB7XG4gICAgICAgIHBhdGg6ICdob21lJyxcbiAgICAgICAgY29tcG9uZW50OiBIb21lQ29tcG9uZW50LFxuICAgICAgICBjaGlsZHJlbjogW1xuICAgICAgICAgICAgeyBwYXRoOiAnJywgY29tcG9uZW50OiBEYXNoYm9hcmRDb21wb25lbnQgfSxcbiAgICAgICAgICAgIHsgcGF0aDogJ2Fib3V0JywgY29tcG9uZW50OiBBYm91dENvbXBvbmVudCB9XG4gICAgICAgIF1cbiAgICB9LFxuICAgIHtcbiAgICAgICAgcGF0aDogJ2RldGFpbCcsXG4gICAgICAgIGNvbXBvbmVudDogRGV0YWlsQ29tcG9uZW50XG4gICAgfVxuXTtcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgICAgICBOYXRpdmVTY3JpcHRNb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZVxuICAgIF0sXG4gICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgIENPTVBPTkVOVFNcbiAgICBdLFxuICAgIGV4cG9ydHM6IFtcbiAgICAgICBDT01QT05FTlRTLFxuICAgICAgICBOYXRpdmVTY3JpcHRNb2R1bGUsXG4gICAgICAgIE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZVxuICAgIF1cbn0pXG5leHBvcnQgY2xhc3MgQ29tcG9uZW50c01vZHVsZSB7IH0iXX0=