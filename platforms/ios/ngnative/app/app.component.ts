import { Component } from "@angular/core";
import { registerElement } from 'nativescript-angular/element-registry';
import { isIOS } from 'platform';
import { topmost } from 'ui/frame';
import * as app from 'application';

var themes = require('nativescript-themes');

registerElement('CheckBox', () => require('nativescript-checkBox').CheckBox)

declare var android;

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent { 
    constructor (){
        if(isIOS){
            topmost().ios.controller.navigationBar.barStyle = 1;
        }
        // else{
        //     let decorView = app.android.startActivity.getWindow().getDecorView();
        //     decorView.setSystemUiVisibility(android.view.View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        // }
    }
}
